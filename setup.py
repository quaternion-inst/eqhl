from setuptools import setup

setup(name='eqhl',
      version='0.2',
      description='A highlighting system to explain equations and their components easily',
      url='https://gitlab.com/trajectory/eqhl',
      author='Spencer Burris',
      author_email='sburris@posteo.net',
      license='ISC',
      packages=['eqhl'],
      zip_safe=False)
