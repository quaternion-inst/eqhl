import unittest

from eqhl import *


class TestEquation(unittest.TestCase):
    def test_newtons(self):
        newtons_second = Equation([
            HLTerm(r'\vec{F}', 'Force'),
            Term('='),
            HLTerm('m', 'Mass'),
            HLTerm(r'\vec{a}', 'Acceleration')
        ])

        expected_html = """<ul class="equation">\n\t<li class="blue" title="Force">\(\\vec{F}\)</li>\n\t<li>\(=\)</li>\n\t<li class="yellow" title="Mass">\(m\)</li>\n\t<li class="cyan" title="Acceleration">\(\\vec{a}\)</li>\n</ul>"""

        self.assertEqual(str(newtons_second.html), expected_html)


if __name__ == '__main__':
    unittest.main()
